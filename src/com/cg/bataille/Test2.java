package com.cg.bataille;

import java.util.Arrays;
import java.util.List;

public class Test2 {
//	J ->11, Q->12, K->13, A->14 
	public static final Card P1_CARD1 = new Card("5", "D");
	public static final Card P1_CARD2 = new Card("3", "D");
	public static final Card P1_CARD3 = new Card("2", "D");
	public static final Card P1_CARD4 = new Card("7", "D");
	public static final Card P1_CARD5 = new Card("8", "D");
	public static final Card P1_CARD6 = new Card("7", "D");
	public static final Card P1_CARD7 = new Card("5", "D");
	public static final Card P1_CARD8 = new Card("5", "D");
	public static final Card P1_CARD9 = new Card("6", "D");
	public static final Card P1_CARD10 = new Card("5", "D");
	public static final Card P1_CARD11 = new Card("4", "D");
	public static final Card P1_CARD12 = new Card("6", "D");
	public static final Card P1_CARD13 = new Card("6", "D");
	public static final Card P1_CARD14 = new Card("3", "D");
	public static final Card P1_CARD15 = new Card("3", "D");
	public static final Card P1_CARD16 = new Card("7", "D");
	public static final Card P1_CARD17 = new Card("4", "D");
	public static final Card P1_CARD18 = new Card("4", "D");
	public static final Card P1_CARD19 = new Card("7", "D");
	public static final Card P1_CARD20 = new Card("4", "D");
	public static final Card P1_CARD21 = new Card("2", "D");
	public static final Card P1_CARD22 = new Card("6", "D");
	public static final Card P1_CARD23 = new Card("8", "D");
	public static final Card P1_CARD24 = new Card("3", "D");
	public static final Card P1_CARD25 = new Card("2", "D");
	public static final Card P1_CARD26 = new Card("2", "D");
//	J ->11, Q->12, K->13, A->14 
	public static final Card P2_CARD1 = new Card("14", "D");
	public static final Card P2_CARD2 = new Card("9", "D");
	public static final Card P2_CARD3 = new Card("13", "D");
	public static final Card P2_CARD4 = new Card("13", "D");
	public static final Card P2_CARD5 = new Card("13", "D");
	public static final Card P2_CARD6 = new Card("13", "D");
	public static final Card P2_CARD7 = new Card("10", "D");
	public static final Card P2_CARD8 = new Card("10", "D");
	public static final Card P2_CARD9 = new Card("9", "D");
	public static final Card P2_CARD10 = new Card("12", "D");
	public static final Card P2_CARD11 = new Card("11", "D");
	public static final Card P2_CARD12 = new Card("10", "D");
	public static final Card P2_CARD13 = new Card("8", "D");
	public static final Card P2_CARD14 = new Card("12", "D");
	public static final Card P2_CARD15 = new Card("11", "D");
	public static final Card P2_CARD16 = new Card("14", "D");
	public static final Card P2_CARD17 = new Card("11", "D");
	public static final Card P2_CARD18 = new Card("14", "D");
	public static final Card P2_CARD19 = new Card("12", "D");
	public static final Card P2_CARD20 = new Card("14", "D");
	public static final Card P2_CARD21 = new Card("11", "D");
	public static final Card P2_CARD22 = new Card("10", "D");
	public static final Card P2_CARD23 = new Card("9", "D");
	public static final Card P2_CARD24 = new Card("8", "D");
	public static final Card P2_CARD25 = new Card("12", "D");
	public static final Card P2_CARD26 = new Card("9", "D");

	public static final List<Card> LIST_CARDS_PLAYER1 = Arrays.asList(P1_CARD1, P1_CARD2, P1_CARD3, P1_CARD4, P1_CARD5,
			P1_CARD6, P1_CARD7, P1_CARD8, P1_CARD9, P1_CARD10, P1_CARD11, P1_CARD12, P1_CARD13, P1_CARD14, P1_CARD15,
			P1_CARD16, P1_CARD17, P1_CARD18, P1_CARD19, P1_CARD20, P1_CARD21, P1_CARD22, P1_CARD23, P1_CARD24,
			P1_CARD25, P1_CARD26);
	public static final List<Card> LIST_CARDS_PLAYER2 = Arrays.asList(P2_CARD1, P2_CARD2, P2_CARD3, P2_CARD4, P2_CARD5,
			P2_CARD6, P2_CARD7, P2_CARD8, P2_CARD9, P2_CARD10, P2_CARD11, P2_CARD12, P2_CARD13, P2_CARD14, P2_CARD15,
			P2_CARD16, P2_CARD17, P2_CARD18, P2_CARD19, P2_CARD20, P2_CARD21, P2_CARD22, P2_CARD23, P2_CARD24,
			P2_CARD25, P2_CARD26);

}
