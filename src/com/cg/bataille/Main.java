package com.cg.bataille;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		Player player1 = new Player(1, 0, TestDeuxBattaillesEnchannee.LIST_CARDS_PLAYER1, new ArrayList<Card>());
		Player player2 = new Player(2, 0, TestDeuxBattaillesEnchannee.LIST_CARDS_PLAYER2, new ArrayList<Card>());

		System.out.println(test(player1, player2));

	}

	static String test(Player player1, Player player2) {
		List<Card> p1Cards = player1.getCards();
		List<Card> p2Cards = player2.getCards();
		int i = 0;
		List<Card> p1CardsWon = new ArrayList<>();
		List<Card> p2CardsWon = new ArrayList<>();
		int manchePl = 0;
		int mancheP2 = 0;

		while (i < p1Cards.size()) {
			if (Integer.parseInt(p1Cards.get(i).getValue()) > Integer.parseInt(p2Cards.get(i).getValue())) {
				manchePl++;
				p1CardsWon.add(player2.getCards().get(i));
				p1CardsWon.add(player1.getCards().get(i));
				player1.setCardsWon(p1CardsWon);
				player1.setManche(manchePl);
				// p1Cards.remove(i);
			} else if (Integer.parseInt(p1Cards.get(i).getValue()) < Integer.parseInt(p2Cards.get(i).getValue())) {
				mancheP2++;
				p2CardsWon.add(player1.getCards().get(i));
				p1CardsWon.add(player2.getCards().get(i));
				player2.setCardsWon(p2CardsWon);
				player2.setManche(mancheP2);
				// p2Cards.remove(i);
			} else { // bataille

			}
			i++;
		}
		System.err.println("NUMERO JOUEUR 1 => " + player1.getNumber());
//		System.err.println("Les cartes gagn�es JOUEUR 1 => " + player1.getCardsWon());
		System.err.println("Manche JOUEUR1 => " + player1.getManche());

		System.err.println("NUMERO JOUEUR 2 => " + player2.getNumber());
//		System.err.println("Les cartes gagn�es JOUEUR 2 => " + player2.getCardsWon());
		System.err.println("Manche JOUEUR2 => " + player2.getManche());
		return null;
	}
}
