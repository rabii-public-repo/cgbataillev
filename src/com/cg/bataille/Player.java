package com.cg.bataille;

import java.util.List;

public class Player {
	private int number;
	private int manche;
	private List<Card> cards;
	private List<Card> cardsWon;

	public Player() {
	}

	public Player(int number, int manche, List<Card> cards, List<Card> cardsWon) {
		this.number = number;
		this.manche = manche;
		this.cards = cards;
		this.cardsWon = cardsWon;
	}

	int getNumber() {
		return number;
	}

	void setNumber(int number) {
		this.number = number;
	}

	List<Card> getCards() {
		return cards;
	}

	void setCards(List<Card> cards) {
		this.cards = cards;
	}

	int getManche() {
		return manche;
	}

	void setManche(int manche) {
		this.manche = manche;
	}

	List<Card> getCardsWon() {
		return cardsWon;
	}

	void setCardsWon(List<Card> cardsWon) {
		this.cardsWon = cardsWon;
	}

	@Override
	public String toString() {
		return "Player [number=" + number + ", manche=" + manche + ", cards=" + cards + ", cardsWon=" + cardsWon + "]";
	}

}
