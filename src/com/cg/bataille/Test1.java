package com.cg.bataille;

import java.util.Arrays;
import java.util.List;

public class Test1 {
	public static final Card CARD1 = new Card("14", "D");
	public static final Card CARD2 = new Card("13", "D");
	public static final Card CARD3 = new Card("12", "D");

	public static final Card CARD4 = new Card("13", "D");
	public static final Card CARD5 = new Card("12", "D");
	public static final Card CARD6 = new Card("11", "D");

	public static final List<Card> LIST_CARDS_PLAYER1 = Arrays.asList(CARD1, CARD2, CARD3);
	public static final List<Card> LIST_CARDS_PLAYER2 = Arrays.asList(CARD4, CARD5, CARD6);

}
