package com.cg.bataille;

public class Card {
	private String value;
	private String color;

	public Card() {
	}

	public Card(String value, String color) {
		this.value = value;
		this.color = color;
	}

	String getValue() {
		return value;
	}

	void setValue(String value) {
		this.value = value;
	}

	String getColor() {
		return color;
	}

	void setColor(String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "Card [value=" + value + ", color=" + color + "]";
	}
}
