package com.cg.bataille;

import java.util.Arrays;
import java.util.List;

public class TestDeuxBattaillesEnchannee {
//	J ->11, Q->12, K->13, A->14 
	public static final Card P1_CARD1 = new Card("8", "D");
	public static final Card P1_CARD2 = new Card("13", "D");
	public static final Card P1_CARD3 = new Card("14", "D");
	public static final Card P1_CARD4 = new Card("12", "D");
	public static final Card P1_CARD5 = new Card("3", "D");
	public static final Card P1_CARD6 = new Card("13", "D");
	public static final Card P1_CARD7 = new Card("14", "D");
	public static final Card P1_CARD8 = new Card("12", "D");
	public static final Card P1_CARD9 = new Card("6", "D");
//	J ->11, Q->12, K->13, A->14 
	public static final Card P2_CARD1 = new Card("8", "D");
	public static final Card P2_CARD2 = new Card("2", "D");
	public static final Card P2_CARD3 = new Card("3", "D");
	public static final Card P2_CARD4 = new Card("4", "D");
	public static final Card P2_CARD5 = new Card("3", "D");
	public static final Card P2_CARD6 = new Card("2", "D");
	public static final Card P2_CARD7 = new Card("3", "D");
	public static final Card P2_CARD8 = new Card("4", "D");
	public static final Card P2_CARD9 = new Card("7", "D");

	public static final List<Card> LIST_CARDS_PLAYER1 = Arrays.asList(P1_CARD1, P1_CARD2, P1_CARD3, P1_CARD4, P1_CARD5,
			P1_CARD6, P1_CARD7, P1_CARD8, P1_CARD9);
	public static final List<Card> LIST_CARDS_PLAYER2 = Arrays.asList(P2_CARD1, P2_CARD2, P2_CARD3, P2_CARD4, P2_CARD5,
			P2_CARD6, P2_CARD7, P2_CARD8, P2_CARD9);

}
